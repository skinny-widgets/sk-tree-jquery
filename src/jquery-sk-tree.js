
import { SkTreeImpl }  from '../../sk-tree/src/impl/sk-tree-impl.js';

export class JquerySkTree extends SkTreeImpl {

    get prefix() {
        return 'jquery';
    }

    get suffix() {
        return 'tree';
    }

}
